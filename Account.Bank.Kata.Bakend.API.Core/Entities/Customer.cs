﻿using System;

namespace Account.Bank.Kata.Bakend.API.Core
{
    public class Customer
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FistName { get; set; }
        public Guid NumberAccount { get; set; }
    }
}
