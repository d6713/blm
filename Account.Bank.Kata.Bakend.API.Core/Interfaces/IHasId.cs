﻿namespace Account.Bank.Kata.Bakend.API.Core
{
    public interface IHasId
    {
        int Id { get; set; }
    }
}
