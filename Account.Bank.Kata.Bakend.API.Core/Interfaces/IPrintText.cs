﻿namespace Account.Bank.Kata.Bakend.API.Core.Interfaces
{
    public interface IPrintText
    {
        void PrintLine(string text);
    }
}
