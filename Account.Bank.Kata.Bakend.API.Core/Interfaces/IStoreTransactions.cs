﻿using System.Collections.Generic;

namespace Account.Bank.Kata.Bakend.API.Core.Interfaces
{
    public interface IStoreTransactions
    {        
        void AjoutDepot(double amount, int customerById);
        void AjoutRetrait(double amount, int customerById);
        List<Account> GetAccountsByCustomerId(int customerId);
    }
}
