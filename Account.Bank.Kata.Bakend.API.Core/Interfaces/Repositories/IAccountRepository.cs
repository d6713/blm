﻿using System.Collections.Generic;

namespace Account.Bank.Kata.Bakend.API.Core.Interfaces
{
    public interface IAccountRepository : ICrudRepository<Account>
    {
        List<Account> GetAccountsByCustomerId(int customerId);
        double GetSoldeOrderByDate(int customerId);
    }
}
