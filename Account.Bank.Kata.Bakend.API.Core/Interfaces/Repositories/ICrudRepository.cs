﻿using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using System.Collections.Generic;

namespace Account.Bank.Kata.Bakend.API.Core
{
    public interface ICrudRepository<T> where T : class, IHasId
    {
        List<T> GetAll();
        T Get(int id);
        T Add(T entity);
        T Update(T entity);
    }
}
