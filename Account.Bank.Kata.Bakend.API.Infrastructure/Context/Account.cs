﻿using Account.Bank.Kata.Bakend.API.Core;
using System;

namespace Account.Bank.Kata.Bakend.API.Infrastructure
{
    public class Account : IHasId
    {
        public int Id { get; set; }
        public string Operation { get; set; }
        public DateTime Date { get; set; }      
        public double Amount { get; set; }      
        public double Solde { get; set; }       
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }        
    }
}
