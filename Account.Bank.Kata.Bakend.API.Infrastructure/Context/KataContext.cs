﻿using Microsoft.EntityFrameworkCore;

namespace Account.Bank.Kata.Bakend.API.Infrastructure
{
    public partial class KataContext : DbContext
    {
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Account> Account { get; set; }        

        public KataContext(DbContextOptions<KataContext> options) : base(options)
        {
        }
    }
}
