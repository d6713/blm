﻿using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Account.Bank.Kata.Bakend.API.Infrastructure.Repositories
{
    public class AccountRepository : CrudRepository<Core.Account>, IAccountRepository
    {
        private readonly IAccountRepository _accountRepository;

        public AccountRepository(KataContext dbContext, IAccountRepository accountRepository) : base(dbContext)
        {
            _accountRepository = accountRepository;
        }   

        public List<Core.Account> GetAccountsByCustomerId(int customerId)
        {
            return  _accountRepository.GetAll()?.Where(x => x.CustomerId == customerId).ToList();
        }

        public double GetSoldeOrderByDate(int customerId)
        {
            return GetAccountsByCustomerId(customerId).OrderByDescending(x => x.Date).First()?.Solde ?? default;
        }
    }
}
