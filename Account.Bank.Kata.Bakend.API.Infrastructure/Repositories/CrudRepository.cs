﻿using Account.Bank.Kata.Bakend.API.Core;
using System.Collections.Generic;
using System.Linq;

namespace Account.Bank.Kata.Bakend.API.Infrastructure.Repositories
{
    public class CrudRepository<T> : ICrudRepository<T> where T : class, IHasId
    {
        protected KataContext _kataContext;

        public CrudRepository(KataContext dbContext)
        {
            _kataContext = dbContext;
        }

        public List<T> GetAll()
        {
            return _kataContext.Set<T>().ToList();
        }

        public T Get(int id)
        {
            return _kataContext.Set<T>().FirstOrDefault(w => w.Id == id);
        }

        public T Add(T entity)
        {
            T entityUpdated = _kataContext.Set<T>().Add(entity).Entity;
            _kataContext.SaveChanges();

            return entityUpdated;
        }

        public T Update(T entity)
        {
            T entityToUpdate = Get(entity.Id);
            if (entityToUpdate != null)
            {
                _kataContext.Entry<T>(entityToUpdate).CurrentValues.SetValues(entity);
                _kataContext.SaveChanges();
            }

            return entityToUpdate;
        }
    }
}
