﻿using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using System;

namespace Account.Bank.Kata.Bakend.API.Service
{
    public class Print : IPrintText
    {
        public void PrintLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}
