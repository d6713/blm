﻿using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using System.Collections.Generic;

namespace Account.Bank.Kata.Bakend.API.Service
{
    public class TransactionStore : IStoreTransactions
    {
        private readonly IAccountRepository _accountRepository;

        public TransactionStore(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public void AjoutDepot(double amount, int customerById)
        {
            var currentSolde = GetLastAccountByDate(customerById);
            if (amount > 0)
            {
                _accountRepository.Add(new Core.Account 
                { 
                    Operation = Operation.depot.ToString(), 
                    Amount = amount, 
                    Solde = currentSolde + amount,
                    Date = System.DateTime.Now, 
                    CustomerId = customerById
                });
            }                        
        }

        public void AjoutRetrait(double amount, int customerById)
        {
            var currentSolde = GetLastAccountByDate(customerById);
            if (amount > 0 && currentSolde >= amount)
            {
                _accountRepository.Add(new Core.Account 
                { 
                    Operation = Operation.retrait.ToString(), 
                    Amount = amount,
                    Solde = currentSolde - amount,
                    Date = System.DateTime.Now, 
                    CustomerId = customerById
                });
            }            
        }

        public List<Core.Account> GetAccountsByCustomerId(int customerId) => _accountRepository.GetAccountsByCustomerId(customerId);
        

        #region private methods

        private double GetLastAccountByDate(int customerById)
        {
            return _accountRepository.GetSoldeOrderByDate(customerById);
        }

        #endregion private methods
    }
}
