﻿using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Acount.Bank.Account.Backend.API.Web.Controllers
{
    [Route("kata/bank")]
    public class AccountController : BaseController
    {
        private readonly IStoreTransactions _storeTransactions;
        private readonly IPrintText _printText;
        public AccountController(IStoreTransactions storeTransactions, IPrintText printText)
        {
            _storeTransactions = storeTransactions;
            _printText = printText;
        }

        /// <summary>
        /// Dépôt de l'argent
        /// </summary>
        /// <param name="queryFiltersModel"></param>
        /// <returns></returns>
        [HttpPost("AjoutDepot")]
        [SwaggerResponse((int)HttpStatusCode.OK, Description = "Dépôt de l'argent is successfully considered as read for the user")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Exception during read Account")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error in Database")]
        public IActionResult AjoutDepot(
            [FromQuery] AccountModel queryFiltersModel)
        {
            _storeTransactions.AjoutDepot(queryFiltersModel.Amount, queryFiltersModel.CustomerId);

            return NoContent();
        }

        /// <summary>
        /// retrait de l'argent
        /// </summary>
        /// <param name="queryFiltersModel"></param>
        /// <returns></returns>
        [HttpPost("AjoutRetrait")]
        [SwaggerResponse((int)HttpStatusCode.OK, Description = "Retrait de l'argent is successfully considered as read for the user")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Exception during read Account")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error in Database")]
        public IActionResult AjoutRetrait(
            [FromQuery] AccountModel queryFiltersModel)
        {
            _storeTransactions.AjoutDepot(queryFiltersModel.Amount, queryFiltersModel.CustomerId);

            return NoContent();
        }

        /// <summary>
        /// Print relevé
        /// </summary>
        /// <param name="queryFiltersModel"></param>
        /// <returns></returns>
        [HttpGet("Print")]
        [SwaggerResponse((int)HttpStatusCode.OK, Description = "l'impression du relevé is successfully considered as read for the user")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Exception during read Account")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error in Database")]
        public IActionResult Print(
            [FromQuery] int customerId)
        {
            var historyLines = string.Join("\n", _storeTransactions.GetAccountsByCustomerId(customerId));

            _printText.PrintLine(historyLines);

            return NoContent();
        }
    }
}
