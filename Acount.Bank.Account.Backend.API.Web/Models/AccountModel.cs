﻿namespace Acount.Bank.Account.Backend.API.Web
{
    public class AccountModel
    {
        public double Amount { get; set; }
        public int CustomerId { get; set; }
    }
}
