using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Acount.Bank.Account.Backend.API.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureKestrel(o => o.AddServerHeader = false);
                });
        }
    }
}
