using Account.Bank.Kata.Bakend.API.Core.Interfaces;
using Account.Bank.Kata.Bakend.API.Infrastructure;
using Account.Bank.Kata.Bakend.API.Infrastructure.Repositories;
using Account.Bank.Kata.Bakend.API.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;

namespace Acount.Bank.Account.Backend.API.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private readonly IWebHostEnvironment _hostingEnvironment;
        public Startup(IWebHostEnvironment env)
        {
            _hostingEnvironment = env;

            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();


            Configuration = builder.Build();
        }
       
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Service
            services.AddTransient<IStoreTransactions, TransactionStore>();
            services.AddTransient<IPrintText, Print>();
           
            //Repository
            services.AddTransient<IAccountRepository, AccountRepository>();

            services.AddMvc().AddNewtonsoftJson();

            //Configure swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = Configuration.GetValue<string>("SwaggerGenerator:Title"),
                    Version = Configuration.GetValue<string>("SwaggerGenerator:Version"),
                    Description = Configuration.GetValue<string>("SwaggerGenerator:Description"),
                });

                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Acount.Bank.Account.Backend.API.Web.xml"));
            });

            // DbContext
            string connection = GetConnectionStrings();
            services.AddScoped(s =>
            {
                DbContextOptionsBuilder<KataContext> dbContextOptionsBuilder = new DbContextOptionsBuilder<KataContext>()
                    .UseSqlServer(connection);
               
                var context = new KataContext(dbContextOptionsBuilder.Options);
                return context;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private string GetConnectionStrings()
        {
            string connectionStrings = Configuration.GetConnectionString("DB_Access");
            if (string.IsNullOrEmpty(connectionStrings))
            {
                string _passwordSqlDb = "brams";
                string _usernameSqlDb = "sa";
                if (!(string.IsNullOrEmpty(_usernameSqlDb) && string.IsNullOrEmpty(_passwordSqlDb)))
                {
                    connectionStrings = @"Server=.;Initial Catalog=Kata_Bank;Persist Security Info=False;User ID=" + _usernameSqlDb + "; Password=" + _passwordSqlDb + ";MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                }
            }
            return connectionStrings;
        }
    }
}
